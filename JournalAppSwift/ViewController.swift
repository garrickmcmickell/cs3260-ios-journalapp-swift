//
//  ViewController.swift
//  JournalAppSwift
//
//  Created by Garrick McMickell on 10/7/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var dictionaries = [[String: String]]()
    
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        loadData()
    }
    
    func loadData() {
        let defaults = UserDefaults.standard
        dictionaries = defaults.object(forKey: "dictionaries") as? [[String: String]] ?? [[String: String]]()
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictionaries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let dictionary = dictionaries[indexPath.row]
        
        cell.textLabel?.text = dictionary["title"]
        
        return cell
    }
    
    @IBAction func addBtnTouched(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "moveToEntries", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "moveToEntries", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "moveToEntries") {
            let evc: EntryViewController = segue.destination as! EntryViewController
            if ((sender) != nil) {
                let ip = sender as! NSIndexPath
                evc.positionInArray = ip
            }
        }
    }
}

