//
//  EntryViewController.swift
//  JournalAppSwift
//
//  Created by Garrick McMickell on 10/7/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class EntryViewController: UIViewController {
    
    var positionInArray: NSIndexPath? = nil
    
    @IBOutlet var textField: UITextField!
    @IBOutlet var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        if(positionInArray != nil) {
            loadData();
        }
    }
    
    func loadData() {
        let defaults = UserDefaults.standard
        var dictionaries = defaults.object(forKey: "dictionaries") as? [[String: String]] ?? [[String: String]]()
        var d = dictionaries[(self.positionInArray?.row)!]
        
        textField.text = d["title"]
        textView.text = d["content"]
    }
    
    @IBAction func saveBtnTouched(_ sender: UIBarButtonItem) {
        let defaults = UserDefaults.standard
        let d: [String: String] = [
            "title": textField.text!,
            "content": textView.text
        ]
        
        var dictionaries: [[String: String]]? = nil
        
        dictionaries = defaults.object(forKey: "dictionaries") as? [[String: String]] ?? [[String: String]]()
        
        if(dictionaries == nil) {
            dictionaries = [[String: String]]()
        }
        
        print("Row ", positionInArray?.row)
        
        if(positionInArray != nil) {
            dictionaries?.remove(at: (positionInArray?.row)!)
            dictionaries?.insert(d, at: (positionInArray?.row)!)
        }
        else {
            dictionaries?.append(d)
        }
        
        defaults.set(dictionaries, forKey: "dictionaries")
        
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
